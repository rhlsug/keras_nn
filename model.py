from keras.models import Sequential
from keras.layers import *
import keras.optimizers


def create_model(n_classes, input_shape):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(2, 2),
                     activation='relu', input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    #model.add(Conv2D(64, (5, 5), activation='elu'))
    #model.add(MaxPooling2D(pool_size=(2, 2)))
    #model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(n_classes, activation='softmax'))

    # Optimizers rmsprop vs Adadelta
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    return model

# score = model.evaluate(x_test, y_test, verbose=0)
# print('Test loss: ', score[0])
# print('Test accuracy: ', score[1])

# model.save('model.h5')
