import os
import sys
import tarfile

import urllib.request

data_url = 'http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz'
data_dir = 'dataset'


def download_and_extract(url, dest):
    def _progress(count, block_size, total_size):
        pb = 100 / (total_size / block_size)
        output = '\r>> Downloading dataset: {:.2f}%'.format(pb * count)
        sys.stdout.write(output)
        sys.stdout.flush()

    if not url:
        return

    if not os.path.exists(dest):
        os.makedirs(dest)

    filename = url.split('/')[-1]
    filepath = dest + filename

    urllib.request.urlretrieve(data_url, filepath, _progress)

    tarfile.open(filepath, 'r:gz').extractall(dest)


def main():
    download_and_extract(data_url, data_dir)


if __name__ == '__main__':
    main()
