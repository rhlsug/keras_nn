#!/usr/bin/env python3

import os
from random import sample

import numpy as np
import librosa
from sklearn.model_selection import train_test_split

# TODO add background noise to the samples
# see https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/speech_commands/input_data.py#L328
# TODO pad samples to get a uniform np-shape


def get_class_names(path):
    return [
        entry.name
        for entry
        in os.scandir(path)
        if entry.is_dir() and entry.name[0] != '_'
    ]


def get_audio_files(path):
    return [
        entry.name
        for entry
        in os.scandir(path)
        if entry.is_file() and entry.name.endswith('.wav')
    ]


def get_processed_files(path):
    return [
        entry.name
        for entry
        in os.scandir(path)
        if entry.is_file() and entry.name.endswith('.npy')
    ]


# mfcc is better for speech recognition
# spec is better for general audio recognition
def wav_to_spectrogram(path, max_len):
    pass


def wav_to_mfcc(path, max_len=11):
    # load waveform (amplitude) and sampling rate
    wave, sr = librosa.load(path, mono=True, sr=None)

    # downsampling?
    wave = wave[::3]

    mfcc = librosa.feature.mfcc(wave, sr=16000)

    if max_len > mfcc.shape[1]:
        pad_width = max_len - mfcc.shape[1]
        mfcc = np.pad(mfcc, pad_width=((0, 0), (0, pad_width)), mode='constant')
    else:
        mfcc = mfcc[:,:max_len]

    return mfcc


def preprocess_data(path_in, path_out):
    if not os.path.exists(path_out):
        os.mkdir(path_out, 0o755)

    # get class names from subdirectory names
    class_names = get_class_names(path_in)

    for i, class_name in enumerate(class_names):

        class_dir_out = os.path.join(path_out, class_name)
        if not os.path.exists(class_dir_out):
            os.mkdir(class_dir_out, 0o755)

        class_dir_in = os.path.join(path_in, class_name)
        file_names = get_audio_files(class_dir_in)

        n_files = len(file_names)

        for j, file_name in enumerate(file_names):
            progress = '{}/{} files in class "{}" processed'.format(j + 1, n_files, class_name)
            if (j + 1) == n_files:
                print('\r' + progress)
            else:
                print('\r{}'.format(progress), end='')

            audio_file = os.path.join(class_dir_in, file_name)
            processed_file = os.path.join(class_dir_out, file_name) + '.npy'

            if os.path.exists(processed_file):
                continue

            # compute mfcc and save it as numpy array
            mfcc = wav_to_mfcc(audio_file)
            np.save(processed_file, mfcc)


def split_test_training(test_ratio, processed_dir, class_names):
    x_test = []
    y_test = []
    x_train = []
    y_train = []

    x = None
    y = None
    for i, class_name in enumerate(class_names):
        class_dir = os.path.join(processed_dir, class_name)

        proc_files = get_processed_files(class_dir)
        n_files = len(proc_files)

        class_vec = []
        for file in proc_files:
            file_path = os.path.join(class_dir, file)
            vec = np.load(file_path)
            class_vec.append(vec)

        class_vec = np.array(class_vec)

        if x is None:
            x = class_vec
        else:
            x = np.vstack((x, class_vec))

        x = np.array(x)
        if y is None:
            y = np.zeros(x.shape[0])
        else:
            y = np.append(y, np.full(class_vec.shape[0], fill_value=i))

        print(x.shape[0])
        print(len(y))

    print('x.shape[0]: ', x.shape[0])
    print('len(y): ', len(y))
    assert x.shape[0] == len(y)
    return train_test_split(x, y, test_size=test_ratio, shuffle=True)

        #n_test_files = int(round(n_files * test_ratio))

        #test_files = sample(proc_files, k=n_test_files)
        #train_files = [file for file in proc_files if file not in test_files]

        #for test_file in test_files:
        #    test = np.load(os.path.join(class_dir, test_file))


if __name__ == '__main__':
    preprocess_data('dataset', 'processed')