import argparse

from keras.utils import to_categorical

import preprocess
import model


def train(args):
    x_train, x_test, y_train, y_test = preprocess.split_test_training(args.test_ratio, 'processed', args.wanted_classes)

    x_train = x_train.reshape(x_train.shape[0], 20, 11, 1)
    x_test = x_test.reshape(x_test.shape[0], 20, 11, 1)
    y_train_hot = to_categorical(y_train)
    y_test_hot = to_categorical(y_test)

    m = model.create_model(len(args.wanted_classes), (20, 11, 1))

    m.fit(x_train, y_train_hot,
          batch_size=100,
          epochs=200,
          verbose=1,
          validation_data=(x_test, y_test_hot))

    m.save('model.h5')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--wanted_classes',
        type=str,
        default='yes,no,up,down,left,right',
        help='classes to use')
    parser.add_argument(
        '--test_ratio',
        type=float,
        default=0.1,
        help='ratio of test to training samples')
    _args = parser.parse_args()
    _args.wanted_classes = _args.wanted_classes.split(',')
    train(_args)

